package com.ridango.payment.controller;

import com.ridango.payment.model.dto.PaymentDTO;
import com.ridango.payment.service.PaymentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping(path = "/payment")
public class PaymentRestController {
    private final PaymentService paymentService;

    public PaymentRestController(PaymentService paymentService) {
        this.paymentService = paymentService;
    }

    @PostMapping
    public ResponseEntity<?> sendPayment(@Valid @RequestBody PaymentDTO paymentDTO) {
        return ok(paymentService.createPayment(paymentDTO));
    }
}
