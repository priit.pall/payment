package com.ridango.payment.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @Digits(integer = 16, fraction = 2, message = "Balance amount can be a maximum of 18 digits long, including 2 decimal points")
    @DecimalMin(value = "0", message = "Account balance cannot be negative")
    private BigDecimal balance;
}
