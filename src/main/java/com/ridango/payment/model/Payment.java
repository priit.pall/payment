package com.ridango.payment.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Getter
@Setter
public class Payment extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "sender_account_id")
    private Account senderAccount;

    @ManyToOne
    @JoinColumn(name = "receiver_account_id")
    private Account receiverAccount;

    @Digits(integer = 16, fraction = 2, message = "Payment amount can be a maximum of 18 digits long, including 2 decimal points")
    @DecimalMin(value = "0", inclusive = false, message = "Payment amount must be bigger than 0")
    @NotNull
    private BigDecimal amount;
}
