package com.ridango.payment.model.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Getter
@Setter
public class PaymentDTO {
    @NotNull
    private Long senderAccountId;

    @NotNull
    private Long receiverAccountId;

    @Digits(integer = 16, fraction = 2, message = "Payment amount can be a maximum of 18 digits long, including 2 decimal points")
    @DecimalMin(value = "0", inclusive = false, message = "Payment amount must be bigger than 0")
    @NotNull
    private BigDecimal amount;
}
