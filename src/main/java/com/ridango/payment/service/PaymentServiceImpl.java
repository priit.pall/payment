package com.ridango.payment.service;

import com.ridango.payment.model.Payment;
import com.ridango.payment.model.dto.PaymentDTO;
import com.ridango.payment.repository.PaymentRepository;
import org.springframework.stereotype.Service;

@Service
public class PaymentServiceImpl implements PaymentService {
    private final PaymentRepository paymentRepository;
    private final AccountService accountService;

    public PaymentServiceImpl(PaymentRepository paymentRepository, AccountService accountService) {
        this.paymentRepository = paymentRepository;
        this.accountService = accountService;
    }

    @Override
    public Long createPayment(PaymentDTO paymentDTO) {
        accountService.createTransaction(paymentDTO.getSenderAccountId(), paymentDTO.getSenderAccountId(), paymentDTO.getAmount());

        Payment payment = new Payment();
        payment.setSenderAccount(accountService.getAccount(paymentDTO.getSenderAccountId()));
        payment.setReceiverAccount(accountService.getAccount(paymentDTO.getReceiverAccountId()));
        payment.setAmount(paymentDTO.getAmount());

        return save(payment).getId();
    }

    private Payment save(Payment payment) {
        return paymentRepository.save(payment);
    }
}
