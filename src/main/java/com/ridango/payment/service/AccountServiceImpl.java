package com.ridango.payment.service;

import com.ridango.payment.model.Account;
import com.ridango.payment.repository.AccountRepository;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import java.math.BigDecimal;

@Service
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void createTransaction(Long senderId, Long receiverId, BigDecimal amount) {
        Account sender = getAccount(senderId);
        Account receiver = getAccount(receiverId);

        if (sender.getBalance().compareTo(amount) < 0) {
            throw new ValidationException("Account does not have enough money for the payment");
        }
        removeFunds(amount, sender);
        addFunds(amount, receiver);
    }

    @Override
    public Account getAccount(Long accountId) {
        return findAccount(accountId);
    }

    private void addFunds(BigDecimal amount, Account account) {
        BigDecimal newBalance = account.getBalance().add(amount);
        account.setBalance(newBalance);
        save(account);
    }

    private void removeFunds(BigDecimal amount, Account account) {
        BigDecimal newBalance = account.getBalance().subtract(amount);
        account.setBalance(newBalance);
        save(account);
    }

    private Account findAccount(Long accountId) {
        return accountRepository.findById(accountId).orElseThrow(() -> new EntityNotFoundException("Account not found with id: " + accountId));
    }

    private Account save(Account account) {
        return accountRepository.save(account);
    }
}
