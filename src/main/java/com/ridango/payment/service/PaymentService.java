package com.ridango.payment.service;

import com.ridango.payment.model.dto.PaymentDTO;

public interface PaymentService {
    Long createPayment(PaymentDTO paymentDTO);
}
