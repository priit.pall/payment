package com.ridango.payment.service;

import com.ridango.payment.model.Account;
import com.ridango.payment.model.dto.PaymentDTO;

import java.math.BigDecimal;

public interface AccountService {
    void createTransaction(Long fromId, Long toId, BigDecimal amount);
    Account getAccount(Long accountId);
}
