# Comments

* I created a new Spring Boot project via https://start.spring.io/ as I was having strange issues with the project provided.
* The application.properties and schema.sql files are practically the same as in the base project. The only difference being - schema.sql table "payment" did not have a column to hold the amount of money paid in said payment so I added it.
* I added ModelMapper by default as I knew I would use DTO-s but I ended up actually not using it.
* Quite a few dependencies have been added which are used for two main things:
    * Validation
    * Unit tests
