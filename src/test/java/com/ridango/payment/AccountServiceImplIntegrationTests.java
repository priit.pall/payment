package com.ridango.payment;

import com.ridango.payment.model.Account;
import com.ridango.payment.repository.AccountRepository;
import com.ridango.payment.service.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.persistence.EntityNotFoundException;
import javax.validation.ValidationException;
import java.math.BigDecimal;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
class AccountServiceImplIntegrationTests {
    @MockBean
    private AccountRepository accountRepository;

    @Autowired
    private AccountService accountService;

    @BeforeEach
    void init() {
		Account sender = new Account();
		sender.setId(1L);
		sender.setName("Peeter");
		sender.setBalance(new BigDecimal("500.00"));

		Account receiver = new Account();
        sender.setId(2L);
		receiver.setName("Karmen");
		receiver.setBalance(new BigDecimal("100.00"));

        Mockito.when(accountRepository.findById(1L)).thenReturn(Optional.of(sender));
        Mockito.when(accountRepository.findById(2L)).thenReturn(Optional.of(receiver));
    }

    @Test
    void whenEnoughMoneyForPayment_thenDoNotThrowException() {
        assertDoesNotThrow(() -> accountService.createTransaction(1L, 2L, new BigDecimal("500.00")));
    }

    @Test
	void whenNotEnoughMoneyForPayment_thenThrowException() {
        BigDecimal amount = new BigDecimal("8000.00");
    	assertThrows(ValidationException.class, () -> accountService.createTransaction(1L, 2L, amount));
	}

    @Test
    void whenAccountExists_thenDoNotThrowException() {
        assertDoesNotThrow(() -> accountService.getAccount(1L));
    }

	@Test
    void whenAccountDoesNotExist_thenThrowException() {
        assertThrows(EntityNotFoundException.class, () -> accountService.getAccount(3L));
    }
}
